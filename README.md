# Loteria Game
Classic Mexican game similar to Bingo. [Link](https://lit-basin-99324.herokuapp.com/) to live app.

## Requirements
* Node
* NPM 

## Run
```bash
npm install
npm run dev
```

## Web app
http://localhost:3000/
