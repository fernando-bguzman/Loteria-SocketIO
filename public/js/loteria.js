var user = {};
var currentBoardId;
var cardsHistory = [];

$(document).ready(function () {
  
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('needs-validation');
  // Loop over them and prevent submission
  Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });

  $("#login-form").submit(function(e){
    e.preventDefault();
    submitLogin();
  });

  $("#boards-form").submit(function(e){
    e.preventDefault();
    submitSelectedBoard();
  });

  $('#modalLogin').modal('show');

});


function submitLogin() {
  $.ajax({
    url: '/v1/store/login',
    type: 'POST',
    dataType: 'json',
    data: $('#login-form').serialize(),
  })
  .done(function(res) {
    $('#login-alerts').empty();
    if (res.error) {
      var html = '<div data-alert class="alert-box alert round">'
                 + 'Error' + '</div>';
      $('#login-alerts').append(html);
    }
    else {
      user = res.user;
      socket = new io();
      //socket.heartbeatTimeout = 5000;
      configureSocket(socket);
      socket.emit('new-user', user);
      $('#btn-login').css('display', 'none');
      $('#current-user').html("<h1>"+user[0]+"</h1>");
      $('#modalLogin').modal('hide');
      $('#modalBoards').modal('show');
    }
  })
  .fail(function() {
    var html = 'Por favor recarge la pagina';
    $('#login-alerts').append(html);
  });
  
}


function configureSocket(socket) {
  socket.on('new-user', handleNewUser);
  socket.on('remove-user', handleRemoveUser);
  socket.on('all-online-users', handleAllUsers);
  socket.on('all-boards', handleAllBoards);
  socket.on('select-board', handleSelectBoard);
  socket.on('start-game', handleStartGame);
  socket.on('next-card', handleNextCard);
}

function handleAllUsers(users) {
  $('#online-userslist').empty();
  var htmluser = '';
  for (var i=0; i<users.length; i++) {
    htmluser = '<li id="'+users[i][0]+'">' + users[i][0] 
    + ' <span style="color:orange;" id="user-status-'+users[i][0]+ '"> Online</span>'
    + '</li>';
    $('#online-userslist').append(htmluser);
  }
}

function handleNewUser() {
  socket.emit('all-boards');  
  socket.emit('all-online-users');
}

function handleRemoveUser() {
  socket.emit('all-online-users');
}

function handleAllBoards(boards){
  $('#all-boards').empty();
  let htmlBoards = '';
  for (let i = boards.length - 1; i >= 0; i--) {
    if (boards[i][1] == 0) {
      htmlBoards = `
        <div class="col">
          <div class="form-check" style="padding: 0;">
            <input class="form-check-input" type="radio" name="boardRadios"
              style="display: none;"
              id="boardRadios-${boards[i][0]}"
              value="${boards[i][0]}"
            >
            <label class="form-check-label" for="boardRadios-${boards[i][0]}">
              <img style="width: 13.76vw; height: 30vh;" 
                src="images/boards/${boards[i][0]}.png"
                id="board-${boards[i][0]}" 
                onclick="clickBoard(this.id)"
              >
            </label>
          </div>
        </div>
      `;
    } else {
      htmlBoards = '<div class="col"><img style="width: 13.76vw; height: 30vh;" src="images/boards/'+ boards[i][0] 
    +'.png" id="'+ boards[i][0] +'"></div>';
    }
    $('#all-boards').append(htmlBoards);
  }
}

function handleSelectBoard(resp) {
  $('#board-alerts').empty();
  if (resp == 1) {
    var html = '<div data-alert class="alert-box alert round">'
               + 'Esa tabla ya esta seleccionada. Por favor prueba con otra.' + '</div>';
    $('#board-alerts').append(html);
    socket.emit('all-boards');
  } else if (resp == 2) {
    $('#modalBoards').modal('hide');
    // TODO: Move this type of buttons to html and use css class to show them
    var htmlbj = '<button class="btn btn-primary" onclick="startGame()">Start</button>';
    $('#btns').html(htmlbj);
  }
}

function handleStartGame(board) {
  $('#my-board').empty();
  for (let i = 0; i < 4; i++) {
    const htmlBoard = `
      <div class="row">
        <div class="board-elem">
          <img class="card-img" src="images/cards/${board[1][i][0]}.png" alt="" onclick="showBottleCap(li${board[1][i][0]}.id)" draggable="false">
          <img class="bottle-cap cap-hide" src="images/bottle-cap.png" alt="" id="li${board[1][i][0]}" name="${board[1][i][0]}" onclick="hideBottleCap(this.id);" />
        </div>
        <div class="board-elem">
          <img class="card-img" src="images/cards/${board[1][i][1]}.png" alt="" onclick="showBottleCap(li${board[1][i][1]}.id)" draggable="false">
          <img class="bottle-cap cap-hide" src="images/bottle-cap.png" alt="" id="li${board[1][i][1]}" name="${board[1][i][1]}" onclick="hideBottleCap(this.id);" />
        </div>
        <div class="board-elem">
          <img class="card-img" src="images/cards/${board[1][i][2]}.png" alt="" onclick="showBottleCap(li${board[1][i][2]}.id)" draggable="false">
          <img class="bottle-cap cap-hide" src="images/bottle-cap.png" alt="" id="li${board[1][i][2]}" name="${board[1][i][2]}" onclick="hideBottleCap(this.id);" />
        </div>
        <div class="board-elem">
          <img class="card-img" src="images/cards/${board[1][i][3]}.png" alt="" onclick="showBottleCap(li${board[1][i][3]}.id)" draggable="false">
          <img class="bottle-cap cap-hide" src="images/bottle-cap.png" alt="" id="li${board[1][i][3]}" name="${board[1][i][3]}" onclick="hideBottleCap(this.id);" />
        </div>
      </div>
    `;
    $('#my-board').append(htmlBoard);
  }
}

function handleNextCard(nextCard) {
  cardsHistory.push(nextCard);
  var htmlc = '<img class="current-card" src="images/cards/'+nextCard+'.png" alt="" draggable="false">';
  $('#current-card').html(htmlc);
}


function clickBoard(id) {
  const str = id.split('-');
  currentBoardId = parseInt(str[1]);
  let imgs = document.getElementsByClassName('board-selected');
  [].filter.call(imgs, function(img) {
    img.classList.remove('board-selected');
  });
  document.getElementById(id).classList.add('board-selected');
}

function submitSelectedBoard() {
  if (typeof currentBoardId === 'undefined') {
    alert('Please, pick one board');
    return;
  }
  socket.emit('select-board', currentBoardId);
}

function startGame() {
  const htmlWin = '<button class="btn btn-primary" onclick="checkWinner()">Claim Win!</button>';
  $('#btns').html(htmlWin);
  socket.emit('start-game', user);
}

function endGame() {
  socket.emit('end-game');
  $('#modalWin').modal('show');
}

function showBottleCap(id){
  document.getElementById(id).classList.remove('cap-hide');
}

function hideBottleCap(id) {
  document.getElementById(id).classList.add('cap-hide');
}

function playAgain() {
  $('#current-card').empty();
  $('#my-board').empty();
  var htmlbb = '<button class="btn btn-primary" onclick="startGame()">Start</button>';
  $('#btns').html(htmlbb);
  $('#modalWin').modal('hide');
}

function validateWinCase(boardSelections, winCase) {
  for (let i = 0, l = winCase.length; i < l; i++) {
    const card = boardSelections[winCase[i]];
    if (!card) return false;
    const cardInHistory = cardsHistory.find(c => c === card.id);
    if(!card.selected || typeof cardInHistory === 'undefined') {
      return false;
    }
  }
  return true;
}

function checkWinner() {
  const BOARD_LENGTH = 16;
  const CASES = [
    [0,1,2,3], // Rows
    [4,5,6,7],
    [8,9,10,11],
    [12,13,14,15],
    [0,4,8,12], // Cols
    [1,5,9,13],
    [2,6,10,14],
    [3,7,11,15],
    [0,5,10,15], // diagonals
    [3,6,9,12],
    [0,1,4,5], // squares
    [1,2,5,6],
    [2,3,6,7],
    [4,5,8,9],
    [5,6,9,10],
    [6,7,10,11],
    [8,9,12,13],
    [9,10,13,14],
    [10,11,14,15],
  ];
  const caps = document.getElementsByClassName('bottle-cap');
  let boardSelections = Array.from(Array(BOARD_LENGTH).keys(), x => null);
  for (let i = 0; i < BOARD_LENGTH; i++) {
    const selection = {selected: false, id: null};
    selection.selected = caps[i].classList.contains('cap-hide') ? false : true;
    selection.id = parseInt(caps[i].name);
    boardSelections[i] = selection;
  }
  for (let i = 0, l = CASES.length; i < l; i++) {
    const passes = validateWinCase(boardSelections, CASES[i]);
    if (passes) {
      endGame();
      break;
    }
  };
  $('#win-alerts').html('Check your board');
}
