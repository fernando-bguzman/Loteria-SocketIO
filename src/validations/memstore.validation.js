const Joi = require('joi');

const login = {
  body: Joi.object().keys({
    username: Joi.string().required(),
  }),
};

module.exports = {
  login,
};