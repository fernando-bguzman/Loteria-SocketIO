class MemStore {

  constructor() {
    this.TOTAL_CARDS = 54;
    this.onlineUsers = [];
    this.orderedCards = Array.from(Array(this.TOTAL_CARDS).keys(), x => x + 1);
    this.shuffledCards = [];
    this.boards = [[3,0],[4,0],[5,0],[6,0],[8,0],[9,0],[10,0]];
    this.boardsWCards = [
      [3, [[2,3,4,5],[7,8,9,10],[12,13,14,15],[17,18,19,20]] ],
      [4, [[43,44,45,21],[52,53,54,26],[7,8,9,31],[16,17,18,36]] ],
      [5, [[22,23,24,25],[27,28,29,30],[32,33,34,35],[37,38,39,40]] ],
      [6, [[21,22,23,24],[30,31,32,33],[39,40,41,42],[48,49,50,51]] ],
      [8, [[42,43,44,45],[47,48,49,50],[52,53,54,1],[40,10,19,20]] ],
      [9, [[41,42,37,38],[50,51,46,47],[5,6,1,2],[14,15,10,11]] ],
      [10, [[39,40,19,20],[48,49,28,29],[3,4,37,38],[12,13,46,47]] ]
    ];
  }

  addUser(body) {
    // TODO: Improve user data structure
    const user = [ body.username, 'notienetabla', 0 ];
    this.onlineUsers.push(user);
    return user;
  }

  removeUserById(id) {
    const user = this.getUserById(id);
    if (typeof user !== 'undefined') {
      if (user[2] != 0) {
        const board = this.getBoardById(user[2]);
        board[1] = 0; // free the board
      }
    }
    return this.onlineUsers.splice(this.onlineUsers.indexOf(id), 1);
  }

  shuffleCards() {
    this.shuffledCards = this.orderedCards.sort(() => Math.random() - 0.5);
  }

  getUserById(id) {
    return this.onlineUsers.find(elem => elem[0] === id);
  }

  getBoardById(id) {
    return this.boards.find(elem => elem[0] === id);
  }

  getBoardWCardsById(id) {
    return this.boardsWCards.find(elem => elem[0] === id);
  }

  selectBoard(boardId, userId) {
    const board = this.getBoardById(boardId);
    const user = this.getUserById(userId);
    if (typeof board === 'undefined' || typeof user === 'undefined') {
      return 0; // 0 = board not found
    } else if (board[1] == 1) {
      return 1; // 1 = board is busy
    } else {
      board[1] = 1; // put the board as busy
      user[1] = 'tienetabla'; // user has table 
      user[2] = boardId; // link the table to user
      return 2; // 2 = board is available
    }
  }

}

const test = new MemStore();

module.exports = test;