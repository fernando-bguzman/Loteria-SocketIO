const http = require('http');
const app = require('./app');
const appServer = http.createServer(app);
const io = require('socket.io')(appServer);
const { MemStore } = require('./models');

const CARD_TIMEOUT = 4000;
var refreshIntervalId = 0;

function pickCard() {
  const card = MemStore.shuffledCards.pop();
  if (typeof card === 'undefined') {
    stopPickingCards();
  } else { 
    io.emit('next-card', card);
  }
}

function stopPickingCards() {
  if (!refreshIntervalId) return;
  clearInterval(refreshIntervalId);
  refreshIntervalId = 0;
}

io.on('connection', function(socket) {
  // TODO: Probably use controllers

  socket.on('disconnect', function() {
    if (socket.user) { 
      MemStore.removeUserById(socket.user[0]);
      io.emit('remove-user', socket.user);
    } else {
      io.emit('remove-user');
    }
  });
  
  socket.on('new-user', function(newUser) {
    socket.user = newUser;
    io.emit('new-user', newUser);
  });

  socket.on('all-online-users', function() {
    socket.emit('all-online-users', MemStore.onlineUsers);
  });

  socket.on('all-boards', function() {
    io.emit('all-boards', MemStore.boards);
  });

  socket.on('select-board', function(boardId) {
    const res = MemStore.selectBoard(boardId, socket.user[0]);
    io.emit('select-board', res);
  });

  socket.on('start-game', function(user) {
    // TODO: Move the selected board logic to its handler
    const userInfo = MemStore.getUserById(user[0]);
    if (typeof userInfo !== 'undefined') {
      const board = MemStore.getBoardWCardsById(userInfo[2]);
      io.emit('start-game', board);
    }
    MemStore.shuffleCards();
    pickCard();
    refreshIntervalId = setInterval(pickCard, CARD_TIMEOUT);
  });
  
  socket.on('end-game', function() {
    stopPickingCards();
    // TODO: Reset all
  });

});


module.exports = {
  appServer,
  io,
};