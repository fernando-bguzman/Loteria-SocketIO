// const httpStatus = require('http-status');
// const ApiError = require('../utils/ApiError');
const { MemStore } = require('../models');

/**
 * Print test body
 * @param {Object} reqBody
 * @returns {Promise<MemStore>}
*/
const login = async (reqBody) => {
  return MemStore.addUser(reqBody);
};

module.exports = {
  login,
};