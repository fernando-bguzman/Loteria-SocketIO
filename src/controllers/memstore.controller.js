const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { memStoreService } = require('../services');

const login = catchAsync(async (req, res) => {
  const user = await memStoreService.login(req.body);
  res.status(httpStatus.CREATED).send({ user });
});

module.exports = {
  login,
};