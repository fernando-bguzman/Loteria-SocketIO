const httpStatus = require('http-status');
const path = require('path');

const PUBLIC = path.join(__dirname, '..', '..', 'public');

const home = (req, res) => {
  const pathFile = path.join(PUBLIC, 'index.html');
  res.status(httpStatus.OK).sendFile(pathFile);
};

const game = (req, res) => {
  const pathFile = path.join(PUBLIC, 'loteria.html');
  res.status(httpStatus.OK).sendFile(pathFile);
};

const loadAsset = (req, res) => {
  const pathFile = path.join(PUBLIC, req.url);
  res.status(httpStatus.OK).sendFile(pathFile);
};

module.exports = {
  home,
  game,
  loadAsset,
};