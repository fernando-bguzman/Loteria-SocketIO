 
const express = require('express');
const validate = require('../../middlewares/validate');
const { memStoreValidation } = require('../../validations');
const { memStoreController } = require('../../controllers');

const router = express.Router();

router.post('/login', validate(memStoreValidation.login), memStoreController.login);


module.exports = router;