const express = require('express');
const memStore = require('./memstore.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/store',
    route: memStore,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router; 
