 
const express = require('express');
const { pageController } = require('../../controllers');

const router = express.Router();

router.get('/', pageController.home);
router.get('/game', pageController.game);
router.get('/css/index.css', pageController.loadAsset);
router.get('/css/bootstrap.min.css', pageController.loadAsset);
router.get('/css/bootstrap.min.css.map', pageController.loadAsset);
router.get('/css/loteria.css', pageController.loadAsset);

router.get('/js/jquery-3.2.1.min.js', pageController.loadAsset);
router.get('/js/jquery-3.5.1.slim.min.js', pageController.loadAsset);
router.get('/js/bootstrap.bundle.min.js', pageController.loadAsset);
router.get('/js/bootstrap.bundle.min.js.map', pageController.loadAsset);
router.get('/js/loteria.js', pageController.loadAsset);

router.get('/images/icon-sm.png', pageController.loadAsset);
router.get('/images/icon-lg.png', pageController.loadAsset);
router.get('/images/btn-play-1.png', pageController.loadAsset);
router.get('/images/bottle-cap.png', pageController.loadAsset);
router.get('/images/background-1.png', pageController.loadAsset);
router.get('/images/background-2.jpg', pageController.loadAsset);
router.get('/images/background-3.jpg', pageController.loadAsset);
router.get('/images/boards/3.png', pageController.loadAsset);
router.get('/images/boards/4.png', pageController.loadAsset);
router.get('/images/boards/5.png', pageController.loadAsset);
router.get('/images/boards/6.png', pageController.loadAsset);
router.get('/images/boards/8.png', pageController.loadAsset);
router.get('/images/boards/9.png', pageController.loadAsset);
router.get('/images/boards/10.png', pageController.loadAsset);
for (const i of Array(54).keys()) {
  router.get(`/images/cards/${i+1}.png`, pageController.loadAsset);
}

module.exports = router;