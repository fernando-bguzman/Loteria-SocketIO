const express = require('express');
const pageRoute = require('./page.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/',
    route: pageRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router; 
